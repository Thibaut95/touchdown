import pandas as pd 
import os.path


df_teams = None
df_players = None

f_teams = './static/teams_infos.csv'
f_players = 'static/players_infos.csv'


def load_teams():
    """ Load Teams from csv file """
    global df_teams
    df_teams = pd.read_csv(f_teams, header=0)


def load_players():
    """ Load player from csv file """
    global df_players
    df_players = pd.read_csv(f_players, header=0)


def get_team(abv):
    """ Return a team based on its abreviation """
    return df_teams.loc[df_teams['Abv'] == abv].to_dict('records')[0]


def get_team_by_conference(conf):
    """ Return a dicionary of teams in a certains conference """
    return df_teams.loc[df_teams['Conference'] == conf].to_dict('record')


def get_team_by_division(div):
    """ Return a dicionary of teams in a certains division """
    return df_teams.loc[df_teams['Division'] == div].to_dict('record')


def get_all_teams():
    """ Return a dictionary of all teams """
    return df_teams.to_dict('records')

def get_all_players():
    """ Return a dictionary of all players """
    return df_players.to_dict('records')


def get_player_with_id(id):
    """ Return a player as a dictionary """
    return df_players.loc[df_players['player_index']==id].to_dict('record')[0]


def get_players_form_team(team):
    """ Return a dictionary of all players from a specific team"""
    return df_players.loc[df_players['Tm'] == team].to_dict('record')


def get_player_by_position(pos):
    """ Return a dictionary of all player with a specific position """
    return df_players.loc[df_players['FantPos'] == pos].to_dict('record')


def get_players_with_position(team, pos):
    """ Return a dictionary of all players from a specific team at a specific position """
    players_team = df_players[df_players['Tm'] == team]
    team_pos = players_team[players_team['FantPos'] == pos]
    return team_pos.to_dict('record')


def init():
    """ load teams and players to dataframe """
    load_teams()
    load_players()


if __name__ == '__main__':
    init()
    print(get_team('SEA'))  
