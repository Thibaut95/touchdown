var simplemaps_usmap_mapdata = {
  main_settings: {
    //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#00000",
    background_transparent: "yes",

    //Label defaults
    label_color: "#002767",
    hide_labels: "no",
    border_color: "white",
    state_description: "",
    state_color: "#dcdcdc",
    state_hover_color: "#a5a7a8",
    state_url: "",
    all_states_inactive: "no",
    location_description: "",
    location_color: "#F1F8FA",
    location_pulse: "yes",
    location_pulse_size: "3",
    location_opacity: ".8",
    location_url: "",
    all_states_zoomable: "yes",
    location_size: "60",
    location_type: "marker",
    all_locations_inactive: "no",
    url_new_tab: "yes",
    auto_load: "yes",

    //Zoom settings
    zoom: "yes",
    initial_zoom: "-1",
    initial_zoom_solo: "yes",

    //Advanced settings
    div: "map",
    state_image_url: "",
    state_image_position: "",
    location_image_url: "",
    label_hover_color: "",
    label_size: "",
    label_font: "",
    popups: "Detect",
    manual_zoom: "yes"
  },
  state_specific: {
    HI: {
      hide: "yes",
      inactive: "no",
      popup: "off"
    },
    AK: {
      hide: "yes",
      inactive: "yes",
      popup: "off"
    },
    FL: {
      hide: "no",
      inactive: "no",
      popup: "off"  
    },
    NH: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    VT: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    ME: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    RI: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    NY: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    PA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    NJ: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    DE: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    MD: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    VA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    WV: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    OH: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    IN: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    IL: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    CT: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    WI: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    NC: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    DC: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    MA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    TN: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    AR: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    MO: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    GA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    SC: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    KY: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    AL: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    LA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    MS: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    IA: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    MN: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    OK: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    TX: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    NM: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    KS: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    NE: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    SD: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    ND: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    WY: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    MT: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    CO: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    UT: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    AZ: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    NV: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    OR: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    WA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    CA: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    MI: {
      hide: "no",
      inactive: "no",
      popup: "off"
    },
    ID: {
      hide: "no",
      inactive: "yes",
      popup: "off"
    },
    GU: {
      hide: "yes",
      inactive: "no",
      popup: "off"
    },
    VI: {
      hide: "yes",
      inactive: "no",
      popup: "off"
    },
    PR: {
      hide: "yes",
      inactive: "no",
      popup: "off"
    },
    MP: {
      hide: "yes",
      inactive: "no",
      popup: "off"
    }
  },
  locations: {
    "0": {
      lat: 42.885,
      lng: -78.878,
      name: "Buffalo, NY",
      description: "Bills",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/BUF.svg",
      url: "/teams/abv/BUF",
      type: "image"
    },
    "1": {
      lat: 25.775,
      lng: -80.198,
      name: "Miami, FL",
      description: "Dolphins",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/MIA.svg",
      url: "/teams/abv/MIA",
      type: "image"
    },
    "2": {
      lat: 40.835,
      lng: -74.098,
      name: "East Rutherford, NJ",
      description: "Jets",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/NYJ.svg",
      url: "/teams/abv/NYJ",
      type: "image"
    },
    "3": {
      lat: 42.064,
      lng: -71.248,
      name: "Foxboro, MA",
      description: "Patriots",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/NE.svg",
      url: "/teams/abv/NWE",
      type: "image"
    },
    "4": {
      lat: 39.107,
      lng: -84.504,
      name: "Cincinnati, OH",
      description: "Bengals",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/CIN.svg",
      url: "/teams/abv/CIN",
      type: "image"
    },
    "5": {
      lat: 41.505,
      lng: -81.691,
      name: "Cleveland, OH",
      description: "Browns",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/CLE.svg",
      url: "/teams/abv/CLE",
      type: "image"
    },
    "6": {
      lat: 39.291,
      lng: -76.609,
      name: "Baltimore, MD",
      description: "Ravens",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/BAL.svg",
      url: "/teams/abv/BAL",
      type: "image"
    },
    "7": {
      lat: 40.439,
      lng: -79.997,
      name: "Pittsburg, PA",
      description: "Steelers",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/PIT.svg",
      url: "/teams/abv/PIT",
      type: "image"
    },
    "8": {
      lat: 39.767,
      lng: -86.15,
      name: "Indianapolis, IN",
      description: "Colts",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/IND.svg",
      url: "/teams/abv/IND",
      type: "image"
    },
    "9": {
      lat: 30.331,
      lng: -81.656,
      name: "Jacksonville, FK",
      description: "Jaguars",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/JAX.svg",
      url: "/teams/abv/JAX",
      type: "image"
    },
    "10": {
      lat: 29.761,
      lng: -95.37,
      name: "Houston, TX",
      description: "Texans",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/HOU.svg",
      url: "/teams/abv/HOU",
      type: "image"
    },
    "11": {
      lat: 36.168,
      lng: -86.778,
      name: "Nashville, TN",
      description: "Titans",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/TEN.svg",
      url: "/teams/abv/TEN",
      type: "image"
    },
    "12": {
      lat: 39.74,
      lng: -104.992,
      name: "Denver, CO",
      description: "Broncos",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/DEN.svg",
      url: "/teams/abv/DEN",
      type: "image"
    },
    "13": {
      lat: 32.716,
      lng: -117.162,
      name: "San Diego, CA",
      description: "Chargers",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/LAC.svg",
      url: "/teams/abv/LAC",
      type: "image"
    },
    "14": {
      lat: 39.103,
      lng: -94.583,
      name: "Kansas City, MO",
      description: "Chiefs",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/KC.svg",
      url: "/teams/abv/KAN",
      type: "image"
    },
    "15": {
      lat: 37.805,
      lng: -122.273,
      name: "Oakland, CA",
      description: "Raiders",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/OAK.svg",
      url: "/teams/abv/OAK",
      type: "image"
    },
    "16": {
      lat: 39.952,
      lng: -75.162,
      name: "Philadelphia, PA",
      description: "Eagles",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/PHI.svg",
      url: "/teams/abv/PHI",
      type: "image"
    },
    "17": {
      lat: 40.835,
      lng: -74.098,
      name: "East Rutherford, NJ",
      description: "Giants",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/NYG.svg",
      url: "/teams/abv/NYG",
      type: "image"
    },
    "18": {
      lat: 38.906,
      lng: -77.017,
      name: "Washington, DC",
      description: "Redskins",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/WAS.svg",
      url: "/teams/abv/WAS",
      type: "image"
    },
    "19": {
      lat: 32.778,
      lng: -96.795,
      name: "Dallas, TX",
      description: "Cowboys",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/DAL.svg",
      url: "/teams/abv/DAL",
      type: "image"
    },
    "20": {
      lat: 41.884,
      lng: -87.632,
      name: "Chicago, IL",
      description: "Bears",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/CHI.svg",
      url: "/teams/abv/CHI",
      type: "image"
    },
    "21": {
      lat: 42.332,
      lng: -83.048,
      name: "Detroit, MI",
      description: "Lions",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/DET.svg",
      url: "/teams/abv/DET",
      type: "image"
    },
    "22": {
      lat: 44.513,
      lng: -88.01,
      name: "Green Bay, WI",
      description: "Packers",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/GB.svg",
      url: "/teams/abv/GNB",
      type: "image"
    },
    "23": {
      lat: 44.979,
      lng: -93.265,
      name: "Minneapolis , MN",
      description: "Vikings",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/MIN.svg",
      url: "/teams/abv/MIN",
      type: "image"
    },
    "24": {
      lat: 27.947,
      lng: -82.459,
      name: "Tampa, FL",
      description: "Buccaneers",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/TB.svg",
      url: "/teams/abv/TAM",
      type: "image"
    },
    "25": {
      lat: 33.748,
      lng: -84.391,
      name: "Atlanta, GA",
      description: "Falcons",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/ATL.svg",
      url: "/teams/abv/ATL",
      type: "image"
    },
    "26": {
      lat: 35.223,
      lng: -80.838,
      name: "Charlotte, NC",
      description: "Panthers",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/CAR.svg",
      url: "/teams/abv/CAR",
      type: "image"
    },
    "27": {
      lat: 29.954,
      lng: -90.078,
      name: "New Orleans, LA",
      description: "Saints",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/NO.svg",
      url: "/teams/abv/NOR",
      type: "image"
    },
    "28": {
      lat: "38.5",
      lng: -122.42,
      name: "San Francisco, CA",
      description: "49ers",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/SF.svg",
      url: "/teams/abv/SFO",
      type: "image"
    },
    "29": {
      lat: 33.539,
      lng: -112.186,
      name: "Glendale, AZ",
      description: "Cardinals",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/ARI.svg",
      url: "/teams/abv/ARI",
      type: "image"
    },
    "30": {
      lat: 38.628,
      lng: -90.2,
      name: "St. Louis, MO",
      description: "Rams",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/LA.svg",
      url: "/teams/abv/LAR",
      type: "image"
    },
    "31": {
      lat: 47.604,
      lng: -122.329,
      name: "Seattle, WA",
      description: "Seahawks",
      image_url: "https://static.nfl.com/static/content/public/static/wildcat/assets/img/logos/teams/SEA.svg",
      url: "/teams/abv/SEA",
      type: "image"
    }
  },
  regions: {
},
  labels: {}
};