# TouchDown


Lien vers la présentation : https://slides.com/jonathanguerne-1/deck

## Installation

### prérequis

- python 3.6 ou +

### Marche à suivre

1. cloner le projet
2. Ouvrir un terminal dans le dossier racine du projet
3. Lancer la commande ```pip install -r requirements.txt```
4. Lancer la commande ```python flask_main.py``` pour lancer le projet

le site sera accesible à l'adresse 127.0.0.1:5000


## Motivation

Le but de l'application est de présenter aux amateurs de NFL une interace leur permettant de 
comparer leurs joueurs et équipes préférées. Le football est un sport qui contient 
une grande quantité de stats très intéressantes à analyser pour les amateurs, cependant ces informations sont parfois 
difficile à appréhender. Le but de ce projet est de pouvoir offir aux utilisateurs une expérience imagée, plus immersive et plus
claire qu'un simple fichier excel.

## Données utilisées

L'idée initiale était de se connecter à une api qui aurait permit au programme d'avoir les informations de la ligue en temps réel.
Malheureusement après un certain temps de recherche nous avons pu constater qu'il était très difficile de trouver une telle api gratuite ou
bon marché. La solution à donc été de se diriger vers le site https://www.pro-football-reference.com qui met à disposition sous forme de fichiers csv
les stats historique de la NFL. Comme cette nouvelle solution rendait difficile la récupération automatique du contenu en temps réel, nous avons décider de nous concentrer sur la saison 2017-1028
car les chiffres ne bougeraient plus.

Au vu de la grande quantité de données existante il a fallu faire des choix. Un premier choix notable est que pour
faciliter le travail d'extraction des données seul 4 postes (de joueurs) on été utilisé dans la version finale. De plus au vu de
la grande quantité de statistiques disponible l'emphase à été mise sur les suivantes :

### Equipes 

* Nom, ville, logo, conférence
* stats
   * victoire/ défaite
   * point marqués
   * passe réussite/ passe tentée
   * nombre de TD total (avec détails)
   * nombre de fieldgoals tentés / réussits

### Joueurs

attention on s’occupe des joueurs QB, RB, WD et TE

* Nom, age, équipe, poste
* stats
   * nombre de matchs joués
   * distance parcouru
   * passes réussite, essayé, nb yard, nb TD, nb interception 
   * réception réussite, réception tentée, nb de yards, nb TD
   * nb TD total

## Représentation

### Page de situation des Equipes

Pour la situation des équipes, il nous a semblé judicieux de les représenter sur une carte de l'amérique, afin de ne pas coder l'information sur la couleur uniquement, la carte est en variation de gris, avec le logo des différentes
équipes, dans les différentes villes qu'elles représentent, ces logos sont en couleurs, mais l'information est ici aussi codé dans la forme, ce qui ne pose pas de problème pour les différents niveau de daltonnisme.
Il est également possible de sélectionner les différentes divisions (AFC et NFC), et de zoomer sur la carte.

### Page de comparaison des Equipes

La page de comparaison des équipes permet d'avoir une représentation de toutes les statistiques d'une équipe sélectionnée. Les statistiques sont répartient en plusieurs blocs pour permettre plus de clareté. Les différents blocs sont : les informations générales, le nombre total de touchdown, le ranking de l'équipe, les rushs, les passes, les field goals.

### Page de formation des Equipes

La page de formation des équipes permet une représentation des joueurs sur le terrain en fonction de leur poste. Une carte interactive permet de sélectionner un poste et ainsi afficher la liste des joueurs de cette équipe ayant évolués à ce poste. Chaque joueurs est représenté sous la forme d'un bloc mettant en avant ces informations générale. En cliquant sur le nom du joueur on est redirigé vers la page d'information du joueur.

### Page d'information du Joueur

La page d'information du joueur permet d'avoir une représentation de toute les statistiques d'un joueur sélectionné. Les statistiques sont séparer en plusieurs blocs pour permettre plus de clareté. Les différents blocs sont : les informations générales, le nombre total de touchdown, les macths joués, les rushs, les passes lancées et les réceptions. La photo du joueur est aussi chargée depuis le site de ressource.

### Page de comparaison des Joueurs

La page de compraison permet de comparer les statistiques de deux joueurs. La page est séparée en deux par une ligne verticale, chaque joueur est assigné à une demi page. Dans chacune des demi pages ont présentent les mêmes statistiques pour permettre une comparaison par simétrie.

## Théorie

### Perception

En général, il a été décidé de mettre le moins de texte possible pour représenter les en-têtes des données, pour privilégier plutôt les images, car 1 image vaut 1000 mots, et que celle-ci est plus vite reconnue qu'un texte lu. Le désavantage de cette méthode, est que les symboles ne sont pas toujours reconnu immédiatement, en tout cas par les néophytes. 

#### Comparateur

Dans les comparateurs, il a été choisi de représenter les données côte à côte, et d'avoir l'entête des différentes statistiques au centre, par des pictogrammes, afin d'éviter la redondance
et de ne pas avoir une trop grande quantité de texte, de plus, le placement au centre aide au traitement pré-attentif des données.
L'on aurait pu faire un système de couleurs en alternance, afin d'aider la lecture des données, cependant, la grande différence des types de données justifie le fait que l'utilisateur n'aie pas besoin de ce guide.

Un des points que l'on pourrait améliorer, est dans la représentation des données dans le comparateur des équipes / joueurs.
en effet, plusieurs améliorations pourraient être mises en place comme par exemple, avoir un pré-traitement attentif des données grâces aux changements suivants:

    - Mettre en gras les valeurs plus élevées
    - Griser les données inutiles

    
### Utilisabilité

[link](https://www.nngroup.com/articles/ten-usability-heuristics/)

Afin d'améliorer notre site, l'on a effectué le test des "10 Usability Heuristics for User Interface Design" de Jakob Nielson,
et il en est ressorti les éléments suivants:

- Visibilité de l'état du système ✓
- Correspondance du système avec le monde réel ✓
- Liberté, contrôle de l'utilisateur ✓
- Cohérence et standards x
- Préventions des erreurs ~
- Reconnaître plutôt que se souvenir  ✓
- Flexibilité et efficacité d'utilisation x
- Esthétique et design minimaliste ~
- Faciliter l'identification et la gestion des erreurs x
- Aide et documentation x

Ce qu'il manque à notre site de manière générale, c'est une manière de montrer comment l'utiliser, une sorte de didacticiel, ainsi que la gestion des erreurs, 
et soigner l'estéthique globale du site, celui-ci paraîssant un peu vide.


### Navigation

En général, il manque à l'utilisateur un fil d'Ariane, afin d'indiquer à l'utilisateur où il est.

### Interaction

sur la page d'accueil, l'on arrive d'abord sur la map, avec les différentes équipes qui y sont représentées.


## Outils utilisés

### Backend, travail sur les données

La majeur partie du travail d'extraction des données a été faite en python avec l'aide de la librairie Pandas. Cette librairie à permis de 
rendre le travail sur les grands fichiers de données csv récupérés depuis notre site de référence très facile.

Le site web est hébergé sur un serveur flask (python) qui a permis au travers de routes et de templates de facilement mettre en communication la partie backend et frontend.


## Améliorations

### Général

- ajout d'un favicon
- bulles d'aides
- design

### Map

- position géographique des équipes

### Formations

- afin d'accentuer l'effet des joueurs cliquable, il pourrait être mis un effet "spotlight" sur le joueur lorsque la souris lui passe dessus.
- ajout d'onglet pour l'affichages des formations défensives et spéciales
- améliorer le toggle (desactiver les autres si on sélectionne un poste)
- ajouter le label de l'âge des joueurs

### Comparateur

- Mettre en évidence les meilleures valeurs


### Conclusion

Notre site, bien que fonctionnel, peut être amélioré et retravaillé, mais grâce notamment au test d'utilisabilité, nous avons excatement les points qui doivent être remis en question, et qui nous
permettrons de corriger le tir. Le projet nous a plus, et nous a permis de travailler sur un sujet qui nous non seulement plaisir, mais également, de découvrir quelques outils 
qui nous seront utile par la suite.