from flask import Flask, render_template, jsonify,url_for
from flask_bootstrap import Bootstrap

import backend

app = Flask(__name__)
Bootstrap(app)


@app.route('/')
def home():
    return render_template('map.html')


@app.route('/map')
def map():
    return render_template('map.html')


@app.route('/compare')
def compare():
    return render_template('compare.html')

@app.route('/formation/<string:team_abv>')
def formation(team_abv):
    team = backend.get_team(team_abv)

    temp_position_list = {'QB':'Quater backs','TE':'Tight ends','RB':'Running backs', 'WR':'Wide receivers'}
    all_positions = []

    for abv, name in temp_position_list.items():
        temp_dic = {}
        temp_dic['id'] = abv
        temp_dic['name'] = name
        temp_dic['players'] = backend.get_players_with_position(team_abv, abv)

        all_positions.append(temp_dic)

    return render_template('formation.html',team=team, all_positions=all_positions)

@app.route('/player/<int:id>')
def player(id):
    
    player = backend.get_player_with_id(id)
    team = backend.get_team(player['Tm'])['Full_Name']

    return render_template('player.html', player=player, team=team)

@app.route('/compare/player1/<int:id1>/player2/<int:id2>')
def compare_players(id1,id2):

    player1 = None
    player2 = None

    team1 = None
    team2 = None

    if id1 < 1000:
        player1 = backend.get_player_with_id(id1)
        team1 = backend.get_team(player1['Tm'])['Full_Name']
    if id2 < 1000:
        player2 = backend.get_player_with_id(id2)
        team2 = backend.get_team(player2['Tm'])['Full_Name']

    all_players = backend.get_all_players()

    return render_template('compareplayers.html', players={"player1":player1,"player2":player2}, teams={"player1":team1,"player2":team2}, all_players=all_players)

@app.route('/compare/team1/<string:abv1>/team2/<string:abv2>')
def compare_teams(abv1,abv2):

    team1 = None
    team2 = None

    if abv1 != "XXX":
        team1 = backend.get_team(abv1)
    if abv2 != "XXX":    
        team2 = backend.get_team(abv2)

    all_teams = backend.get_all_teams()

    return render_template('compareteams.html', teams={"team1":team1,"team2":team2}, all_teams=all_teams)



@app.route('/teams/abv/<string:abv>')
def get_team(abv):
    team = backend.get_team(abv)
    print(team)
    return render_template('team.html', team=team) 


@app.route('/teams/conference/<string:conf>')
def get_teams_by_conference(conf):
    return jsonify(backend.get_team_by_conference(conf))


@app.route('/teams/division/<string:div>')
def get_teams_by_divison(div):
    return jsonify(backend.get_team_by_division(div))


@app.route('/teams')
def get_all_teams():
    return jsonify(backend.get_all_teams())


@app.route('/players/team/<string:team>')
def get_players_form_team(team):
    players = backend.get_players_form_team(team)
    return jsonify(players)


@app.route('/players/pos/<string:pos>')
def get_player_by_position(pos):
    return jsonify(backend.get_player_by_position(pos))


@app.route('/players/team/<string:team>/pos/<string:pos>')
def get_players_with_position(team, pos):
    return jsonify(backend.get_players_with_position(team, pos))

    
if __name__ == '__main__':
    backend.init()
    app.run(debug=True)